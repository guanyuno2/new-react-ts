import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import { googleLogout, useGoogleLogin, GoogleLogin } from '@react-oauth/google';
import { hasGrantedAllScopesGoogle, hasGrantedAnyScopeGoogle } from '@react-oauth/google';
interface TokenResponse {
	/** The access token of a successful token response. */
	access_token: string;
	/** The lifetime in seconds of the access token. */
	expires_in: number;
	/** The hosted domain the signed-in user belongs to. */
	hd?: string;
	/** The prompt value that was used from the possible list of values specified by TokenClientConfig or OverridableTokenClientConfig */
	prompt: string;
	/** The type of the token issued. */
	token_type: string;
	/** A space-delimited list of scopes that are approved by the user. */
	scope: string;
	/** The string value that your application uses to maintain state between your authorization request and the response. */
	state?: string;
	/** A single ASCII error code. */
	// error?: ErrorCode;
	/**	Human-readable ASCII text providing additional information, used to assist the client developer in understanding the error that occurred. */
	error_description?: string;
	/** A URI identifying a human-readable web page with information about the error, used to provide the client developer with additional information about the error. */
	error_uri?: string;
}


function App() {
	const [tokenResponse, setTokenResponse] = useState<TokenResponse>({} as TokenResponse)

	useEffect(() => {
		const hasAccess =
			hasGrantedAllScopesGoogle(
				tokenResponse,
				'google-scope-1',
				'google-scope-2',
			);

		const hasAccessAny = hasGrantedAnyScopeGoogle(
			tokenResponse,
			'google-scope-1',
			'google-scope-2',
		);

		console.log("ALOO", { hasAccess, hasAccessAny });

	}, [])

	const responseMessage = (response: any) => {
		console.log("responseMessage", response);
	};
	const errorMessage = (error: any) => {
		console.log("errorMessage", error);
	};
	const onClickLogout = () => {
		googleLogout()
	}

	const onLogin = useGoogleLogin({
		onSuccess: tokenResponse => console.log(tokenResponse),
	});


	return (
		<div>
			ty van new version
		</div>
	)
}
export default App;

