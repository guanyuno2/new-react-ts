
export const BaseUrl = "https://api.zcfposqn.com" //dev
// export const BaseUrl = "http://localhost:8080" //dev

export const api_call = async (method: string, body: any, path: string): Promise<any> => {
    let url = `${BaseUrl}${path}`;
    
    try {
        let rp = await fetch(url, {
            method: method,
            body: body ? JSON.stringify(body) : null,
            credentials: 'include',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });
        let data = await rp.json();
        return data
    }
    catch (e) {
        // window.alert("Xin lỗi, đã có lỗi khi thực hiện");

        throw e;
    }
    finally {

    }
}

export const api_call_get = (path: string): Promise<any> => {
    return api_call('GET', null, path);
}

export const api_call_post = (path: string, body: any): Promise<any> => {
    return api_call('POST', body, path);
}

export const api_call_put = (path: string, body: any): Promise<any> => {
    return api_call('PUT', body, path);
}

export const api_call_delete = (path: string): Promise<any> => {
    return api_call('DELETE', null, path);
}