// @flow 
import * as React from 'react';
type Props = {
    children: React.ReactNode
};
export const Layout = (props: Props) => {
    return (
        <div>
            {props.children}
        </div>
    );
};