import { Route, Routes } from "react-router"
import { Layout } from "./layout/Layout/Layout"
import { Dashboard } from "./screen/Dashboard/Dashboard"




export const routes = <Layout >
    <Routes>
        <Route path="/" element={<Dashboard />} />
    </Routes>
</Layout>